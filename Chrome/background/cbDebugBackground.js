// cbDebug - Debug parameter enabler extension (background event handlers)
// (C) 2019 by Winni Neessen <winfried.neessen@cleverbridge.com>

"use strict";

// Local cached storage for easy setting access
var cacheStorage = {};
chrome.storage.local.get(['extOn', 'debugMode'], function(storObj) {
    cacheStorage.extOn = storObj.extOn;
    cacheStorage.debugMode = storObj.debugMode;
});
chrome.storage.onChanged.addListener(function() {
    chrome.storage.local.get(['extOn', 'debugMode'], function(storObj) {
        cacheStorage.extOn = storObj.extOn;
        cacheStorage.debugMode = storObj.debugMode;
    })
});

// Change browser icon on status change
chrome.storage.onChanged.addListener(function(changeObj, nameSpace) {
    let extOn = changeObj.extOn;
    if(typeof extOn !== 'undefined' && extOn !== null) {
        if(extOn.newValue === true) {
            chrome.browserAction.setIcon({path: 'img/cbdebug.png'});
        }
        else if(extOn.newValue === false) {
            chrome.browserAction.setIcon({path: 'img/cbdebugoff.png'});
        }
    }
});

chrome.webRequest.onBeforeSendHeaders.addListener(function(reqDetails) {
    if(reqDetails.type === 'main_frame') {
        if(cacheStorage.extOn === true) {
            let requestHeaders = reqDetails.requestHeaders;
            let requestUrl = new URL(reqDetails.url);
            requestHeaders.push({
                name: 'Authorization',
                value: 'Basic QWxhZGRpbjpPcGVuU2VzYW1l'
            });
            requestHeaders.push({
                name: 'X-CB-Debug',
                value: cacheStorage.debugMode
            });

            return {requestHeaders: requestHeaders};
        }
    }
}, {urls: ['<all_urls>']}, ['requestHeaders', 'blocking']);