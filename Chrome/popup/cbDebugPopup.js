// cbDebug - Debug parameter enabler extension (popup menu JS)
// (C) 2019 by Winni Neessen <winfried.neessen@cleverbridge.com>

"use strict";

document.addEventListener('DOMContentLoaded', function () {
    let enableBtn = document.querySelector('#enableBtn');

    // First check if the extension is already enabled
    chrome.storage.local.get(['extOn'], function(returnObj) {
        if(returnObj.extOn === true) {
            enableBtn.dispatchEvent(new MouseEvent('click'));
            chrome.storage.local.get(['debugMode'], function(debugObj) {
                let debugBtn = document.querySelector('input[name="debugParm"][value="' + debugObj.debugMode + '"]');
                if(typeof debugBtn !== 'undefined' && debugBtn !== null) {
                    debugBtn.dispatchEvent(new MouseEvent('click'));
                }
            });
        }
    });

    // We need to update the storage on enable/disable events
    enableBtn.addEventListener('click', function() {
        chrome.storage.local.set({extOn: enableBtn.checked});
    });
    enableBtn.addEventListener('change', function () {
        // Enable all debugParm buttons
        [].forEach.call(document.querySelectorAll('input[name="debugParm"]:disabled'), function(searchElm) {
            searchElm.disabled = false;
        });

        // Enable the last debug mode
        chrome.storage.local.get(['debugMode'], function(debugObj) {
            let debugBtn = document.querySelector('input[name="debugParm"][value="' + debugObj.debugMode + '"]');
            if(typeof debugBtn !== 'undefined' && debugBtn !== null) {
                debugBtn.dispatchEvent(new MouseEvent('click'));
            }
        });

        // Update setting in user store
        chrome.storage.local.set({extEnable: false});

        // Control the enable/disable button
        enableBtn.addEventListener('click', function (clickEvnt) {
            if(enableBtn.checked === true) {
                [].forEach.call(document.querySelectorAll('input[name="debugParm"]'), function(searchElm) {
                    searchElm.checked = false;
                    searchElm.disabled = true;
                });
                chrome.storage.local.set({extOn: false});
                enableBtn.checked = false;
            }
        }, {once: true});

        // Update the storage when settings are changed
        [].forEach.call(document.querySelectorAll('input[name="debugParm"]'), function(searchElm) {
            searchElm.addEventListener('input', function() {
                chrome.storage.local.set({debugMode: searchElm.value});
            });
        });
    });
});